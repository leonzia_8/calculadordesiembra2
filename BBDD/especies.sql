-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-06-2021 a las 20:33:14
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `especies`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especies`
--

CREATE TABLE `especies` (
  `id` int(11) NOT NULL,
  `especie` varchar(20) NOT NULL,
  `distanciamiento` float NOT NULL,
  `fecha` varchar(30) NOT NULL,
  `dias_cosecha` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `especies`
--

INSERT INTO `especies` (`id`, `especie`, `distanciamiento`, `fecha`, `dias_cosecha`) VALUES
(1, 'acelga', 0.2, '2,3,4,5,6,8,9,10,11,12', 80),
(2, 'ajo', 0.15, '3,4,5', 270),
(3, 'apio', 0.25, '1,2,3,9,10,11,12', 150),
(4, 'esparrago', 0.3, '8,9', 1825),
(5, 'espinaca', 0.1, '2,3,4,5,6', 90),
(6, 'lechuga', 0.2, '8,9,10,11,12', 90),
(7, 'papa', 0.3, '1,2,8,9', 150),
(8, 'pimiento', 0.45, '9', 200),
(9, 'rabanito', 0.05, '2,3,4,5,6,9,10,11,12', 40),
(10, 'remolacha', 0.1, '3,4,5,6,8,9,10,11,12', 130);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `especies`
--
ALTER TABLE `especies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `especies`
--
ALTER TABLE `especies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
