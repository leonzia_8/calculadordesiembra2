package edu.neo.entity;

import org.springframework.stereotype.Component;

@Component
public class RespuestaEntity {

	private String especie;
	private float largo;
	private float ancho;
	private int cantPlantines;
	private String fechaCosecha;

	public float getLargo() {
		return largo;
	}

	public void setLargo(float largo) {
		this.largo = largo;
	}

	public float getAncho() {
		return ancho;
	}

	public void setAncho(float ancho) {
		this.ancho = ancho;
	}

	public int getCantPlantines() {
		return cantPlantines;
	}

	public void setCantPlantines(int cantPlantines) {
		this.cantPlantines = cantPlantines;
	}

	public String getFechaCosecha() {
		return fechaCosecha;
	}

	public void setFechaCosecha(String fechaCosecha) {
		this.fechaCosecha = fechaCosecha;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

}