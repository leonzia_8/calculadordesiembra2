package edu.neo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "especies")

public class EspecieEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int id;
	@Column
	private String especie;
	@Column
	private float distanciamiento;
	@Column
	private String fecha;
	@Column
	private int dias_cosecha;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public float getDistanciamiento() {
		return distanciamiento;
	}

	public void setDistanciamiento(float distanciamiento) {
		this.distanciamiento = distanciamiento;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getDiasCosecha() {
		return dias_cosecha;
	}

	public void setDiasCosecha(int diasCosecha) {
		this.dias_cosecha = diasCosecha;
	}

}
