package edu.neo.entity;

import org.springframework.stereotype.Component;

@Component
public class RespuestaErrorEntity {
	private String mensaje;

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
