package edu.neo.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.neo.entity.RespuestaEntity;
import edu.neo.entity.RespuestaErrorEntity;
import edu.neo.repository.EspecieRepository;
import edu.neo.servicios.EspecieServicio;

@CrossOrigin
@RestController
public class EspecieControlador {

	@Autowired
	private EspecieRepository repository;

	@Autowired
	private RespuestaEntity respuesta;

	@Autowired
	private RespuestaErrorEntity respuestaError;

	@GetMapping("/saludo")
	public String saludo() {
		return "hola mundillo--";
	}

	@GetMapping("/getAllEspecies")
	public ResponseEntity<?> getAllEspecies() {
		try {
			return ResponseEntity.ok(this.repository.findAll());
		} catch (Exception e) {
			System.out.println("error:" + e);
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("especie/{id}")
	public ResponseEntity<?> getEspecie(@PathVariable("id") int id,
			@RequestParam(value = "largo", defaultValue = "0") String largo,
			@RequestParam(value = "ancho", defaultValue = "0") String ancho) {
		float miLargo = Float.parseFloat(largo);
		float miAncho = Float.parseFloat(ancho);

		try {
			EspecieServicio servicio = new EspecieServicio(miAncho, miLargo, this.repository.findById(id).get(),
					this.respuesta);
			if (servicio.calculadorSiembra()) {
				return ResponseEntity.ok(this.respuesta);
			} else {
				this.respuestaError.setMensaje(" La especie consultada está fuera de calendario");
				return ResponseEntity.ok(this.respuestaError);
			}
		} catch (Exception e) {
			this.respuestaError.setMensaje("No se encontraron datos de dicha especie");
			return ResponseEntity.ok(this.respuestaError);
		}

	}

}