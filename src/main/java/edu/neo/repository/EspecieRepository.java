package edu.neo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.neo.entity.EspecieEntity;

@Repository
public interface EspecieRepository extends JpaRepository<EspecieEntity, Integer> {

}
