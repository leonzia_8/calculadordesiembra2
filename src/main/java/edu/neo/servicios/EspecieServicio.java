package edu.neo.servicios;

import java.time.LocalDate;

import edu.neo.entity.EspecieEntity;
import edu.neo.entity.RespuestaEntity;

public class EspecieServicio {

	private float largo;
	private float ancho;
	private EspecieEntity especie;
	private RespuestaEntity respuestaEntity;

	public EspecieServicio(float largo, float ancho, EspecieEntity especie, RespuestaEntity respuestaEntity) {
		this.largo = largo;
		this.ancho = ancho;
		this.especie = especie;
		this.respuestaEntity = respuestaEntity;
	}

	public boolean calculadorSiembra() {

		float distancia = 0f;
		float cantPlantines = 0;
		LocalDate fechaHoy = LocalDate.now();
		LocalDate fechaCosecha;
		int mesActual = LocalDate.now().getMonthValue();
		String mesesCosecha;
		boolean sePuedeSembrarHoy = false;
		String[] arrayFechas;

		distancia = this.especie.getDistanciamiento();
		cantPlantines = (this.largo / distancia) * (this.ancho / distancia);
		mesesCosecha = this.especie.getFecha();

		String[] parts = mesesCosecha.split(",");
		int meses[] = new int[parts.length];
		for (int i = 0; i < parts.length; i++) {
			meses[i] = Integer.parseInt(parts[i]);
		}

		for (int i = 0; i < meses.length; i++) {
			if (mesActual == meses[i]) {
				sePuedeSembrarHoy = true;
			}
		}

		if (sePuedeSembrarHoy) {
			fechaCosecha = fechaHoy.plusDays(this.especie.getDiasCosecha());

			String auxiliarFechaCosecha = fechaCosecha + "";
			arrayFechas = auxiliarFechaCosecha.split("-");
			String aux = arrayFechas[2] + "/" + arrayFechas[1] + "/" + arrayFechas[0];

			this.respuestaEntity.setEspecie(this.especie.getEspecie());
			this.respuestaEntity.setLargo(this.largo);
			this.respuestaEntity.setAncho(this.ancho);
			this.respuestaEntity.setCantPlantines(Math.round(cantPlantines));
			this.respuestaEntity.setFechaCosecha(aux);

			return true;
		} else {
			return false;
		}
	}

}
